package com.appspot.magtech.kphoenix

import com.appspot.magtech.kphoenix.components.flattabpane.FlatTabPane
import javafx.scene.Scene
import javafx.stage.Stage
import org.junit.jupiter.api.Test
import org.testfx.framework.junit5.ApplicationTest
import org.testfx.framework.junit5.Start

class FlatTabPaneTest: ApplicationTest() {

    @Start
    override fun start(stage: Stage) {
        stage.scene = Scene(FlatTabPane(), 600.0, 600.0)
        stage.show()
    }

    @Test
    fun testShow() {

    }
}