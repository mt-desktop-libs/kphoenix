package com.appspot.magtech.kphoenix

import com.appspot.magtech.kphoenix.components.initialsava.InitialsAvaStyle.Companion.initialsAva
import com.appspot.magtech.kphoenix.components.initialsava.initialsava
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*

class InitialsAvaPreview: App(InitialsAvaView::class)

class InitialsAvaView: View() {

    init {
        importStylesheet<InititalsAvaViewStyles>()
    }

    override val root = flowpane {
        addClass(InititalsAvaViewStyles.initialsAvaPreviewRoot)
        label("Ivanov Ivan")
        initialsava("Ivanov") {

        }
    }
}

class InititalsAvaViewStyles: Stylesheet() {

    companion object {
        val initialsAvaPreviewRoot by cssclass()
    }

    init {
        initialsAvaPreviewRoot {
            prefWidth = 400.px
            prefHeight = 400.px
            alignment = Pos.CENTER
            hgap = 10.px

            initialsAva {
                prefHeight = 35.px
                prefWidth = 35.px
                label {
                    textFill = Color.RED
                }
            }
        }
    }
}