package com.appspot.magtech.kphoenix

import com.appspot.magtech.kphoenix.components.toast.ErrorToast
import com.appspot.magtech.kphoenix.components.toast.InfoToast
import com.appspot.magtech.kphoenix.components.toast.ToastStyle.Companion.toastError
import com.appspot.magtech.kphoenix.components.toast.ToastStyle.Companion.toastInfo
import com.appspot.magtech.kphoenix.components.toast.showSnackbar
import com.appspot.magtech.kphoenix.extensions.kfxbutton
import com.appspot.magtech.kphoenix.values.LabelFont
import com.appspot.magtech.kphoenix.values.labelFont
import javafx.geometry.Orientation
import javafx.geometry.Pos
import tornadofx.*

class ToastsPreview: App(ToastView::class)

class ToastView: View("Toast") {

    init {
        importStylesheet<ToastsViewStyles>()
    }

    override val root = flowpane {
        prefWidth = 600.0
        prefHeight = 600.0
        alignment = Pos.CENTER
        orientation = Orientation.HORIZONTAL
        hgap = 20.0
        kfxbutton ("Show error") {
            action {
                app.showSnackbar(ErrorToast("Error!"))
            }
        }
        kfxbutton ("Show info") {
            action {
                app.showSnackbar(InfoToast("Some info"))
            }
        }
    }
}

class ToastsViewStyles: Stylesheet() {

    companion object {

    }

    init {
        toastError {
            prefWidth = 300.px
        }

        toastInfo {
            prefHeight = 100.px
            alignment = Pos.BOTTOM_CENTER
            label {
                labelFont = LabelFont(
                    size = 18.0
                )
            }
        }
    }
}