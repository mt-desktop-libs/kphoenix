package com.appspot.magtech.kphoenix

import com.appspot.magtech.kphoenix.components.erroralert.ErrorAlertStyle.Companion.errorAlert
import com.appspot.magtech.kphoenix.components.erroralert.showErrorAlert
import com.appspot.magtech.kphoenix.extensions.kfxbutton
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*

class ErrorAlertPreview: App(ErrorAlertView::class)

class ErrorAlertView: View() {

    init {
        importStylesheet<ErrorAlertViewStyles>()
    }

    override val root = flowpane {
        prefWidth = 600.0
        prefHeight = 400.0
        alignment = Pos.CENTER
        kfxbutton("Show alert") {
            action {
                app.showErrorAlert("Test message")
            }
        }
    }
}

class ErrorAlertViewStyles: Stylesheet() {

    companion object {

    }

    init {
        errorAlert {
            content {
                backgroundColor += Color.GREEN
            }
        }
    }
}