package com.appspot.magtech.kphoenix

import com.appspot.magtech.kphoenix.components.flattabpane.FlatTabPaneStyle.Companion.flatTabPane
import com.appspot.magtech.kphoenix.components.flattabpane.FlatTabPaneStyle.Companion.selectedTab
import com.appspot.magtech.kphoenix.components.flattabpane.flattabpane
import com.appspot.magtech.kphoenix.components.flattabpane.tab
import com.appspot.magtech.kphoenix.components.toast.InfoToast
import com.appspot.magtech.kphoenix.components.toast.showSnackbar
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*

class FlatTabPanePreview: App(FlatTabPaneView::class)

class FlatTabPaneView: View("FlatTabPane") {

    init {
        importStylesheet<Styles>()
    }

    override val root = stackpane {
        addClass(Styles.flatTabPanePreviewRoot)
        flattabpane {
            prefWidth = 350.0
            prefHeight = 350.0
            arrayOf(1, 2, 3).forEach {
                tab(TabView("Content $it")) {
                    text = "tab $it"
                    action {
                        if (isSelected) {
                            app.showSnackbar(InfoToast("Tab $it selected"))
                        }
                    }
                }
            }
        }
    }

    class TabView(private val name: String): View() {

        override val root = flowpane {
            addClass(Styles.contentRoot)
            alignment = Pos.CENTER
            label(name)
        }
    }
}

class Styles: Stylesheet() {

    companion object {
        val flatTabPanePreviewRoot by cssclass()
        val flatTabPaneCustom by cssclass()
        val contentRoot by cssclass()
    }

    init {
        flatTabPanePreviewRoot {

            flatTabPane {
                backgroundColor += Color.GREEN
                tabHeaderArea {
                    prefHeight = 30.px
                    button {
                        backgroundColor += Color.AZURE
                    }
                    selectedTab {
                        backgroundColor += Color.BURLYWOOD
                    }
                }
            }
            contentRoot {
                backgroundColor += Color.BURLYWOOD
            }
        }
    }
}