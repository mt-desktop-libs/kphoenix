package com.appspot.magtech.kphoenix

import com.appspot.magtech.kphoenix.extensions.kfxRadius
import com.appspot.magtech.kphoenix.extensions.kfxbutton
import com.appspot.magtech.kphoenix.wrappers.fetchingview.FetchingViewStyle.Companion.fetchingView
import com.appspot.magtech.kphoenix.wrappers.fetchingview.fetchingView
import com.appspot.magtech.observables.delegates.ObservableVar
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.paint.Color
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tornadofx.*

class FetchingViewPreview: App(FetchingViewView::class)

class FetchingViewView: View() {

    init {
        importStylesheet<FetchingViewViewStyles>()
    }

    private var fetching by ObservableVar(false)

    private fun loadingProcess() {
        CoroutineScope(Dispatchers.IO).launch {
            CoroutineScope(Dispatchers.Main).launch {
                fetching = true
            }
            delay(3000)
            CoroutineScope(Dispatchers.Main).launch {
                fetching = false
            }
        }
    }

    override val root = flowpane {
        addClass(FetchingViewViewStyles.fetchingViewViewRoot)
        fetchingView<LoadingView>(::fetching)
        kfxbutton("Start Loading") {
            action {
                loadingProcess()
            }
        }
    }
}

class LoadingView: View() {

    override val root = flowpane {
        prefWidth = 100.0
        prefHeight = 100.0
        alignment = Pos.CENTER
        style {
            backgroundColor += Color.AQUA
        }
        label("Loading")
    }
}

class FetchingViewViewStyles: Stylesheet() {

    companion object {
        val fetchingViewViewRoot by cssclass()
    }

    init {
        fetchingViewViewRoot {
            orientation = Orientation.VERTICAL
            vgap = 20.px
            alignment = Pos.CENTER
            prefWidth = 400.px
            prefHeight = 400.px

            fetchingView {
                backgroundColor += c("#0000FF", 0.5)
                alignment = Pos.TOP_CENTER
                alignment
                progressIndicator {
                    kfxRadius = 50.px
                    println(kfxRadius)
                }
            }
        }
    }
}