package com.appspot.magtech.kphoenix.wrappers.fetchingview

import com.appspot.magtech.kphoenix.values.Colors
import javafx.geometry.Pos
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.cssid

class FetchingViewStyle: Stylesheet() {

    companion object {
        const val spinnerRadius = 15.0

        val fetchingView by cssid()
        val maskRoot by cssclass()
    }

    init {
        maskRoot {
            alignment = Pos.CENTER
            backgroundColor += Colors.maskDark
        }
    }
}