package com.appspot.magtech.kphoenix.wrappers

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.event
import javafx.scene.Parent
import javafx.scene.layout.StackPane

abstract class StatefullView<S>(initState: S): StackPane() {

    var state: S by ObservableVar(initState)
    abstract fun render(): Parent

    fun updateState(block: (S) -> S) {
        state = block(state)
    }

    fun _render() {
        this.children.clear()
        render()
    }

    init {
        ::_render.event(::state)
    }
}