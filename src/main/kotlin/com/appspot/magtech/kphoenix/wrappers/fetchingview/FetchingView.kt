package com.appspot.magtech.kphoenix.wrappers.fetchingview

import com.appspot.magtech.kphoenix.extensions.kfxspinner
import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.addListener
import com.appspot.magtech.observables.extensions.bind
import javafx.event.EventTarget
import tornadofx.*
import kotlin.reflect.KMutableProperty0

class FetchingView<C: UIComponent, T>(fetching: KMutableProperty0<T>,
                                   component: C,
                                   fromTarget: (T) -> Boolean): View() {

    init {
        importStylesheet<FetchingViewStyle>()
    }

    private var rootWidth by ObservableVar(0.0)
    private var rootHeight by ObservableVar(0.0)

    override val root = stackpane {
        add(component)
        widthProperty().addListener { _, _, newValue ->
            rootWidth = newValue.toDouble()
        }
        heightProperty().addListener { _, _, newValue ->
            rootHeight = newValue.toDouble()
        }
    }

    private fun showMask() {
        flowpane {
            setId(FetchingViewStyle.fetchingView)
            addClass(FetchingViewStyle.maskRoot)
            bind("prefWidth", ::rootWidth)
            bind("prefHeight", ::rootHeight)
            kfxspinner {
                radius = FetchingViewStyle.spinnerRadius
            }
        }
    }

    init {
        fetching.addListener { newValue ->
            if (!fromTarget(prevValue) && fromTarget(newValue)) {
                showMask()
            } else if (fromTarget(prevValue) && !fromTarget(newValue)) {
                root.children.removeAt(root.children.size - 1)
            }
        }
        if (fromTarget(fetching.get())) {
            showMask()
        }
    }
}

inline fun <reified T: UIComponent> EventTarget.fetchingView(fetching: KMutableProperty0<Boolean>) =
        FetchingView(fetching, find(T::class)) { it }.root.attachTo(this)

fun EventTarget.fetchingView(fetching: KMutableProperty0<Boolean>, component: UIComponent) =
        FetchingView(fetching, component) { it }.root.attachTo(this)

fun <T> EventTarget.fetchingView(fetching: KMutableProperty0<T>,
                                 component: UIComponent,
                                 fromTarget: (T) -> Boolean) =
        FetchingView(fetching, component, fromTarget).root.attachTo(this)