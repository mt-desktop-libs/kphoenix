package com.appspot.magtech.kphoenix.wrappers

import javafx.scene.Parent
import tornadofx.Stylesheet
import tornadofx.View
import tornadofx.importStylesheet
import kotlin.reflect.KClass

class ModelView<T>(block: View.(T) -> Parent, model: T): View() {

    constructor(block: View.(T) -> Parent,
                model: T,
                style: KClass<out Stylesheet>): this(block, model) {
        importStylesheet(style)
    }

    override val root = block(model)
}

fun <T> modelView(block: View.(T) -> Parent): (T) -> View =
    { model: T ->
        ModelView(block, model)
    }

inline fun <T, reified S: Stylesheet> modelStyledView(noinline block: View.(T) -> Parent): (T) -> View =
    { model: T ->
        ModelView(block, model, S::class)
    }