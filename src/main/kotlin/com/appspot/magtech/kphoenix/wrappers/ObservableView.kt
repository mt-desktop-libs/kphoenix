package com.appspot.magtech.kphoenix.wrappers

import com.appspot.magtech.observables.extensions.event
import javafx.scene.Parent
import tornadofx.Stylesheet
import tornadofx.View
import tornadofx.importStylesheet
import tornadofx.stackpane
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty0

class ObservableView<T>(private val property: KMutableProperty0<T>,
                        private val block: View.(T) -> Parent): View() {

    constructor(property: KMutableProperty0<T>,
                block: View.(T) -> Parent,
                style: KClass<out Stylesheet>): this(property, block) {
        importStylesheet(style)
    }

    override val root = stackpane {
        add(block(property.get()))
    }

    fun render() {
        root.children.clear()
        root.add(block(property.get()))
    }

    init {
        this::render.event(property)
    }
}

fun <T> observableView(block: View.(T) -> Parent): (KMutableProperty0<T>) -> View =
        { property: KMutableProperty0<T> ->
            ObservableView(property, block)
        }

inline fun <T, reified S: Stylesheet> observableStyledView(noinline block: View.(T) -> Parent): (KMutableProperty0<T>) -> View =
        { property: KMutableProperty0<T> ->
            ObservableView(property, block, S::class)
        }