package com.appspot.magtech.kphoenix.components.initialsava

import com.appspot.magtech.kphoenix.values.Colors
import com.appspot.magtech.kphoenix.values.Fonts
import com.appspot.magtech.kphoenix.values.labelFont
import javafx.geometry.Pos
import javafx.scene.text.TextAlignment
import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.cssid

class InitialsAvaStyle: Stylesheet() {

    companion object {
        val initialsAva by cssid()
        val initialsAvaRoot by cssclass()
        val initials by cssclass()
    }

    init {
        initialsAvaRoot {
            backgroundColor += Colors.primeYellow

            initials {
                labelFont = Fonts.initialsFont
                textAlignment = TextAlignment.CENTER
                alignment = Pos.CENTER
            }
        }
    }
}