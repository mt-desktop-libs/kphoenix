package com.appspot.magtech.kphoenix.components.erroralert

import com.appspot.magtech.kphoenix.extensions.kfxbutton
import javafx.scene.layout.Priority
import javafx.stage.StageStyle
import tornadofx.*
import tornadofx.Stylesheet.Companion.content

class ErrorAlert(text: String): View() {

    init {
        importStylesheet<ErrorAlertStyle>()
    }

    override val root = vbox {
        setId(ErrorAlertStyle.errorAlert)
        addClass(ErrorAlertStyle.errorAlertRoot)
        flowpane {
            addClass(content)
            vgrow = Priority.ALWAYS
            label(text)
        }
        hbox {
            addClass(ErrorAlertStyle.buttonBox)
            stackpane { hgrow = Priority.ALWAYS }
            kfxbutton("ОК") {
                action {
                    currentStage?.close()
                }
            }
        }
    }
}

fun App.showErrorAlert(text: String) {
    ErrorAlert(text).openModal(stageStyle = StageStyle.TRANSPARENT, block = true)
}