package com.appspot.magtech.kphoenix.components.toast

import com.jfoenix.controls.JFXSnackbar
import javafx.scene.layout.Pane
import javafx.util.Duration
import tornadofx.*
import kotlin.reflect.KClass

open class Toast(text: String,
                 style: KClass<out Stylesheet>,
                 rootStylesheet: CssRule,
                 elementId: CssRule): View() {

    init {
        importStylesheet(style)
    }

    override val root = flowpane {
        addClass(rootStylesheet)
        setId(elementId)
        label(text)
    }
}

class ErrorToast(text: String): Toast(
    text,
    ToastStyle::class,
    ToastStyle.toastErrorRoot,
    ToastStyle.toastError
)

class InfoToast(text: String): Toast(
    text,
    ToastStyle::class,
    ToastStyle.toastInfoRoot,
    ToastStyle.toastInfo
)



enum class ShowDuration(val interval: Duration) {
    QUICK(Duration(1000.0)),
    MEDIUM(Duration(1500.0)),
    FAST(Duration(3500.0))
}

inline fun <reified T: UIComponent> App.showSnackbar(duration: ShowDuration = ShowDuration.MEDIUM)
        = this.showSnackbar(find(T::class), duration)

fun App.showSnackbar(uiComponent: UIComponent, duration: ShowDuration = ShowDuration.MEDIUM) {
    val snackbar = JFXSnackbar(find(primaryView).root as Pane)
    snackbar.enqueue(JFXSnackbar.SnackbarEvent(uiComponent.root, duration.interval, null))
}