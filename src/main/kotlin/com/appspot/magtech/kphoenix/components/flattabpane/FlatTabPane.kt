package com.appspot.magtech.kphoenix.components.flattabpane

import com.appspot.magtech.kphoenix.extensions.kfxbutton
import com.appspot.magtech.kphoenix.wrappers.StatefullView
import com.appspot.magtech.observables.extensions.bind
import javafx.event.EventTarget
import javafx.scene.Node
import javafx.scene.layout.Priority
import tornadofx.*
import tornadofx.Stylesheet.Companion.tabHeaderArea
import kotlin.reflect.KClass

class FlatTab(var text: String = "", val content: Node) {

    private var _action: () -> Unit = {}
    var isSelected = false

    val action: () -> Unit
        get() = _action

    fun action(op: () -> Unit) { _action = op }
}

data class FlatTabPaneState(val tabs: List<FlatTab>, val selectedTab: FlatTab?)

class FlatTabPane: StatefullView<FlatTabPaneState>(
        FlatTabPaneState(listOf(), null)
    ) {

    init {
        importStylesheet<FlatTabPaneStyle>()
    }

    fun addTab(text: String = "", content: Node, op: FlatTab.() -> Unit) {
        updateState {
            val newTab = FlatTab(text, content)
            newTab.op()
            FlatTabPaneState(
                tabs = state.tabs.plusElement(newTab),
                selectedTab = state.selectedTab ?: newTab
            )
        }
    }

    override fun render() =
        vbox {
            widthProperty().addListener { _, _, newValue ->
                FlatTabPaneStyle.rootWidth = newValue.toDouble()
            }
            addClass(FlatTabPaneStyle.flatTabPaneRoot)
            setId(FlatTabPaneStyle.flatTabPane)
            hbox {
                addClass(tabHeaderArea)
                state.tabs.forEach { tab ->
                    kfxbutton(tab.text) {
                        if (state.selectedTab == tab) {
                            addClass(FlatTabPaneStyle.selectedTab)
                        }
                        bind("prefWidth", FlatTabPaneStyle.Companion::rootWidth) { it / 3 }
                        action {
                            updateState {
                                FlatTabPaneState(
                                    tabs = state.tabs.map { it.isSelected = false; it },
                                    selectedTab = tab
                                )
                            }
                            tab.isSelected = true
                            tab.action()
                        }
                    }
                }
            }
            stackpane {
                vgrow = Priority.ALWAYS
                if (state.selectedTab != null) {
                    add(state.selectedTab!!.content)
                }
            }
        }
}

fun EventTarget.flattabpane(op: FlatTabPane.() -> Unit = {}) =  FlatTabPane().attachTo(this, op)

inline fun <reified  T: UIComponent> FlatTabPane.tab(noinline op: FlatTab.() -> Unit = {}) = tab(T::class, op)
fun FlatTabPane.tab(uiComponent: KClass<out UIComponent>, op: FlatTab.() -> Unit = {}) = tab(find(uiComponent), op)

fun FlatTabPane.tab(uiComponent: UIComponent, op: FlatTab.() -> Unit = {}): FlatTab {
    addTab(content = uiComponent.root, op = op)
    return state.tabs.last()
}