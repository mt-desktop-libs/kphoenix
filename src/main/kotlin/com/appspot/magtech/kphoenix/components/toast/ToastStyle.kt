package com.appspot.magtech.kphoenix.components.toast

import com.appspot.magtech.kphoenix.values.Colors
import com.appspot.magtech.kphoenix.values.Fonts
import com.appspot.magtech.kphoenix.values.labelFont
import javafx.geometry.Pos
import tornadofx.*

class ToastStyle: Stylesheet() {

    companion object {
        const val toastHeight = 40

        val toastErrorRoot by cssclass()
        val toastError by cssid()
        val toastInfoRoot by cssclass()
        val toastInfo by cssid()
    }

    init {
        val toastRoot = mixin {
            alignment = Pos.TOP_CENTER
            backgroundRadius += box(10.px)
            padding = box(8.px, 15.px, 5.px, 15.px)
            prefHeight = toastHeight.px
            prefWidth = 0.px
        }

        toastErrorRoot {
            +toastRoot
            backgroundColor += Colors.maskDark

            label {
                labelFont = Fonts.errorFont
            }
        }

        toastInfoRoot {
            +toastRoot
            backgroundColor += Colors.maskSemiDark

            label {
                labelFont = Fonts.infoFont
            }
        }
    }
}