package com.appspot.magtech.kphoenix.components.initialsava

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.bind
import javafx.event.EventTarget
import javafx.scene.layout.AnchorPane
import tornadofx.*

class InitialsAva(name: String): AnchorPane() {

    var name by ObservableVar(name)

    init {
        importStylesheet<InitialsAvaStyle>()
        addClass(InitialsAvaStyle.initialsAvaRoot)
        setId(InitialsAvaStyle.initialsAva)
        this.prefWidthProperty().addListener { _, _, newValue ->
            style = "-fx-background-radius: ${newValue.toInt() / 2}px"
        }
        this.prefHeightProperty().addListener { _, _, newValue ->
            style = "-fx-background-radius: ${newValue.toInt() / 2}px"
        }
        label {
            addClass(InitialsAvaStyle.initials)
            anchorpaneConstraints {
                topAnchor = 0.0
                rightAnchor = 0.0
                bottomAnchor = 0.0
                leftAnchor = 0.0
            }
            bind("text", this@InitialsAva::name) {
                if (it.isNotEmpty()) { it[0].toString().toUpperCase() } else ""
            }
        }
    }
}

fun EventTarget.initialsava(name: String = "", op: InitialsAva.() -> Unit = {}) = InitialsAva(name).attachTo(this, op)