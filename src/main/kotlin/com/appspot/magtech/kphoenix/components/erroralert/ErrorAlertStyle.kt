package com.appspot.magtech.kphoenix.components.erroralert

import com.appspot.magtech.kphoenix.values.Colors
import com.appspot.magtech.kphoenix.values.Fonts
import com.appspot.magtech.kphoenix.values.labelFont
import javafx.geometry.Pos
import tornadofx.*

class ErrorAlertStyle: Stylesheet() {

    companion object {

        val errorAlert by cssid()
        val errorAlertRoot by cssclass()
        val buttonBox by cssclass()
    }

    init {
        errorAlertRoot {
            prefWidth = 300.px
            prefHeight = 150.px
            alignment = Pos.CENTER
            backgroundColor += Colors.deepDark
            label {
                labelFont = Fonts.errorAlertText
            }
            content {
                alignment = Pos.CENTER
            }

            buttonBox {
                padding = box(0.px, 15.px, 10.px, 15.px)

                button {
                    prefWidth = 100.px
                    prefHeight = 10.px
                    borderRadius += box(4.px)
                    borderWidth += box(1.px)
                    borderColor += box(Colors.beetroot)
                    labelFont = Fonts.errorAlertButton
                }
            }
        }
    }
}