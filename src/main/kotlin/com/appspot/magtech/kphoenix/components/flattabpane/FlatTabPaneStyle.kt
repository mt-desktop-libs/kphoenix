package com.appspot.magtech.kphoenix.components.flattabpane

import com.appspot.magtech.observables.delegates.ObservableVar
import javafx.scene.paint.Color
import tornadofx.*

class FlatTabPaneStyle: Stylesheet() {

    companion object {
        const val headerHeight = 60

        var rootWidth by ObservableVar(0.0)

        val flatTabPane by cssid()
        val flatTabPaneRoot by cssclass()
        val selectedTab by cssclass()
        val content by cssclass()
    }

    init {
        flatTabPaneRoot {
            backgroundColor += Color.RED
            tabHeaderArea {
                prefHeight = headerHeight.px
                backgroundColor += Color.YELLOW
                button {
                    backgroundColor += Color.BLUE
                    prefHeight = headerHeight.px
                    borderWidth += box(0.px, 0.px, 1.px, 0.px)
                    borderColor += box(backgroundColor.elements.getOrNull(0) ?: Color.BLUE)
                    borderRadius += box(0.px)
                    backgroundRadius += box(0.px)
                }
                selectedTab {
                    borderColor += box(backgroundColor.elements.getOrNull(0) ?: Color.BLUE,
                        backgroundColor.elements.getOrNull(0) ?: Color.BLUE,
                            Color.CORAL,
                        backgroundColor.elements.getOrNull(0) ?: Color.BLUE)
                }
            }
            content {
                backgroundColor += Color.CYAN
            }
        }
    }
}