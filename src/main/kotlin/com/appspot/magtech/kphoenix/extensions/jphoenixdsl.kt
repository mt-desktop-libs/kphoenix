package com.appspot.magtech.kphoenix.extensions

import com.jfoenix.controls.*
import javafx.collections.ObservableList
import javafx.event.EventTarget
import javafx.scene.Node
import tornadofx.SortedFilteredList
import tornadofx.attachTo

fun EventTarget.kfxtabpane(op: JFXTabPane.() -> Unit = {}) =  JFXTabPane().attachTo(this, op)

fun <T> EventTarget.kfxlistview(values: ObservableList<T>? = null, op: JFXListView<T>.() -> Unit = {}) = JFXListView<T>().kfxAttachTo(this, op) {
    if (values != null) {
        if (values is SortedFilteredList<T>) values.bindTo(it)
        else it.items = values
    }
}

fun EventTarget.kfxprogressbar(initialValue: Double? = null, op: JFXProgressBar.() -> Unit = {}) = JFXProgressBar().kfxAttachTo(this, op) {
    if (initialValue != null) it.progress = initialValue
}

fun EventTarget.kfxspinner(op: JFXSpinner.() -> Unit = {}) = JFXSpinner().attachTo(this, op)

fun EventTarget.kfxbutton(text: String = "", graphic: Node? = null, op: JFXButton.() -> Unit = {}) = JFXButton(text).kfxAttachTo(this, op) {
    if (graphic != null) it.graphic = graphic
}



internal inline fun <T : Node> T.kfxAttachTo(
    parent: EventTarget,
    after: T.() -> Unit,
    before: (T) -> Unit
) = this.also(before).attachTo(parent, after)