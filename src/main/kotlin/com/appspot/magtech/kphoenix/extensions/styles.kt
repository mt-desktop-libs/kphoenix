package com.appspot.magtech.kphoenix.extensions

import tornadofx.CssSelectionBlock
import tornadofx.Dimension
import tornadofx.PropertyHolder

@Suppress("UNCHECKED_CAST")
var CssSelectionBlock.kfxRadius: Dimension<Dimension.LinearUnits>
    get() = properties["-jfx-radius"]?.first as Dimension<Dimension.LinearUnits>
    set(value) {
        setProperty(PropertyHolder.CssProperty("-jfx-radius", false), value)
    }