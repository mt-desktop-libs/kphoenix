package com.appspot.magtech.kphoenix.values

import tornadofx.c

internal object Colors {

    val primeYellow = c("#ffcc00")
    val maskDark = c("#080808", 0.5)
    val maskSemiDark = c("#b0b0b0", 0.5)

    val deepDark = c("#333333")
    val beetroot = c("#ab6061")
}