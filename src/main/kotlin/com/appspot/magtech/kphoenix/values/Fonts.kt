package com.appspot.magtech.kphoenix.values

import javafx.scene.paint.Color
import javafx.scene.paint.Paint
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight
import tornadofx.CssSelectionBlock
import tornadofx.c
import tornadofx.px

data class LabelFont(val fontFamily: String = "System",
                     val size: Double,
                     val fontWeight: FontWeight = FontWeight.NORMAL,
                     val fontStyle: FontPosture = FontPosture.REGULAR,
                     val color: Paint = Color.BLACK)

var CssSelectionBlock.labelFont: LabelFont
    get() = LabelFont(fontFamily = fontFamily,
                      size = fontSize.value,
                      fontWeight = fontWeight,
                      fontStyle = fontStyle,
                      color = textFill)
    set(value) {
        fontFamily = value.fontFamily
        fontStyle = value.fontStyle
        fontWeight = value.fontWeight
        fontSize = value.size.px
        textFill = value.color
    }

internal object Fonts {

    val initialsFont = LabelFont(
        fontWeight = FontWeight.EXTRA_BOLD,
        size = 24.0,
        color = c("#000000")
    )
    val errorFont = LabelFont(
        size = 14.0,
        color = c("#ff0000")
    )
    val infoFont = LabelFont(
        size = 14.0
    )
    val errorAlertText = LabelFont(
        size = 18.0,
        color = Colors.beetroot
    )
    val errorAlertButton = LabelFont(
        size = 14.0,
        color = Colors.beetroot
    )
}