import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    val kotlin_version = "1.3.41"

    repositories {
        mavenLocal()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")

        classpath("org.junit.platform:junit-platform-gradle-plugin:1.1.0")
        classpath("org.junit.platform:junit-platform-launcher:1.1.0")
    }
}

plugins {
    application
    `maven-publish`
    kotlin("jvm") version "1.3.41"
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

val sourceSets = the<SourceSetContainer>()

sourceSets {
    create("preview") {
        java.srcDir(file("src/preview/java"))
        resources.srcDir(file("src/preview/resources"))
        compileClasspath += sourceSets["main"].output + configurations["compileClasspath"]
        runtimeClasspath += output + compileClasspath
    }
}

tasks.register<JavaExec>("preview") {
    group = "verification"
    classpath = sourceSets["preview"].runtimeClasspath
    main = project.properties["mainClassName"].toString()
}

val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks
val compilePreviewKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions.jvmTarget = "1.8"
compileTestKotlin.kotlinOptions.jvmTarget = "1.8"
compilePreviewKotlin.kotlinOptions.jvmTarget = "1.8"

repositories {
    jcenter()
    mavenLocal()
    mavenCentral()
}

val tornadofx_version = "1.7.17"
val junit_version = "5.5.1"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    // coroutines only for debugging
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.3.0-RC")
    
    implementation("no.tornado:tornadofx:$tornadofx_version")
    implementation("com.jfoenix:jfoenix:8.0.8")

    implementation("com.appspot.magtech:observables:2.1")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit_version")
    testImplementation("org.testfx:testfx-core:4.0.16-alpha")
    testImplementation("org.testfx:testfx-junit5:4.0.16-alpha")
    // testImplementation("me.carltonwhitehead.tornadofx-junit5:tornadofx-junit5:1.1")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junit_version")
}

val includedLibs = arrayListOf("jfoenix-8.0.8.jar")

tasks.named<Jar>("jar") {
    configurations["compileClasspath"].forEach { file: File ->
        if (file.name in includedLibs) {
            from(zipTree(file.absoluteFile))
        }
    }
}

publishing {
    repositories {
        maven("https://mymavenrepo.com/repo/2adloi83ZbzFqdzrVebr/")
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.appspot.magtech"
            artifactId = "kphoenix"
            version = "1.2"

            from(components["kotlin"])
        }
    }
}

application {
    mainClassName = project.properties["mainClassName"].toString()
}